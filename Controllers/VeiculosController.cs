using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using efveiculosengrev.Models;
using Microsoft.Extensions.Logging;

namespace efveiculosengrev.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VeiculosController : ControllerBase
    {
        private readonly IT11Context _context;
        private readonly ILogger<VeiculosController> _logger;
        public VeiculosController(IT11Context context, ILogger<VeiculosController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Veiculos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Veiculo>>> GetVeiculos()
        {
            return await _context.Veiculos.AsNoTracking().ToListAsync();
        }

        // GET: api/Veiculos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Veiculo>> GetVeiculo(string id)
        {
            var veiculo = await _context.Veiculos.FindAsync(id);

            if (veiculo == null)
            {
                return NotFound();
            }

            return veiculo;
        }

        // PUT: api/Veiculos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVeiculo(string id, Veiculo veiculo)
        {
            if (id != veiculo.Placa)
            {
                return BadRequest();
            }

            _context.Entry(veiculo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VeiculoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Veiculos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Veiculo>> PostVeiculo(Veiculo veiculo)
        {
            _context.Veiculos.Add(veiculo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VeiculoExists(veiculo.Placa))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetVeiculo", new { id = veiculo.Placa }, veiculo);
        }

        // DELETE: api/Veiculos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Veiculo>> DeleteVeiculo(string id)
        {
            var veiculo = await _context.Veiculos.FindAsync(id);
            if (veiculo == null)
            {
                return NotFound();
            }

            _context.Veiculos.Remove(veiculo);
            await _context.SaveChangesAsync();

            return veiculo;
        }

        // DELETE: api/Veiculos/km/0
        [HttpDelete("km/{km}")]
        public async Task DeleteVeiculoPorKm(decimal km)
        {
            using (var transacao = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var veiculos = _context.Veiculos.Where(v => v.Km == km);
                    _context.RemoveRange(veiculos);
                    await _context.SaveChangesAsync();
                    await transacao.CommitAsync();
                }
                catch (System.Exception)
                {
                    await transacao.RollbackAsync();
                    throw;
                }
            }
        }

        private bool VeiculoExists(string id)
        {
            return _context.Veiculos.Any(e => e.Placa == id);
        }
    }
}
