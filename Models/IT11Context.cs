﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace efveiculosengrev.Models
{
    public partial class IT11Context : DbContext
    {
        public IT11Context()
        {
        }

        public IT11Context(DbContextOptions<IT11Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Veiculo> Veiculos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Veiculo>(entity =>
            {
                entity.HasKey(e => e.Placa);

                entity.ToTable("VEICULOS");

                entity.Property(e => e.Placa)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasColumnName("placa")
                    .IsFixedLength(true);

                entity.Property(e => e.Ano)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("ano");

                entity.Property(e => e.Km)
                    .HasColumnType("numeric(6, 0)")
                    .HasColumnName("km");

                entity.Property(e => e.Marca)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("marca");

                entity.Property(e => e.Modelo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("modelo");

                entity.Property(e => e.Timestamp)
                    .IsRowVersion();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
