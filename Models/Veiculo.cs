﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efveiculosengrev.Models
{
    public partial class Veiculo
    {
        public string Placa { get; set; }
        public decimal Ano { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public decimal? Km { get; set; }
        public byte[] Timestamp {get;set;}
    }
}
